const assert = require('assert')
const FSM = require('../lib/fsm')

describe('FSM callbacks', () => {
  let fsm, state, current

  beforeEach(() => {
    fsm = new FSM('pending')
    fsm.when('confirm', {pending: 'confirmed'})
    fsm.when('reset', {confirmed: 'pending'})

    fsm.on('pending', () => { state = 'Pending' })
    fsm.on('confirmed', () => { state = 'Confirmed' })
    fsm.onAny(() => { current = state })
  })

  it('executes callbacks when entering a state', () => {
    fsm.trigger('confirm')
    assert.equal(state, 'Confirmed')

    fsm.trigger('reset')
    assert.equal(state, 'Pending')
  })

  it('executes the callback on any transition', () => {
    fsm.trigger('confirm')
    assert.equal(current, 'Confirmed')

    fsm.trigger('reset')
    assert.equal(current, 'Pending')
  })

  it('passing the event name to the callbacks', () => {
    let eventName

    fsm = new FSM('pending')
    fsm.when('confirm', {pending: 'confirmed'})

    fsm.on('confirmed', (event) => {
      eventName = event
    })

    fsm.trigger('confirm')

    assert.equal(eventName, 'confirm')
  })
})
