const assert = require('assert')
const FSM = require('../lib/fsm')

describe('FSM transitions', () => {
  let fsm

  beforeEach(() => {
    fsm = new FSM('pending')
    fsm.when('confirm', {pending: 'confirmed'})
    fsm.when('ignore', {pending: 'ignored'})
    fsm.when('reset', {confirmed: 'pending', ignored: 'pending'})
  })

  it('defines initial state', () => {
    assert.equal(fsm.state, 'pending')
  })

  it('preserves the state if transition is not possible', () => {
    assert(!fsm.doesTrigger('reset'))
    assert(!fsm.trigger('reset'))
    assert.equal(fsm.state, 'pending')
  })

  it('changes the state if transition is possible', () => {
    assert(fsm.doesTrigger('confirm'))
    assert(fsm.trigger('confirm'))
    assert.equal(fsm.state, 'confirmed')
  })

  it('discerns multiple transitions', () => {
    fsm.trigger('confirm')
    assert.equal(fsm.state, 'confirmed')

    fsm.trigger('reset')
    assert.equal(fsm.state, 'pending')

    fsm.trigger('ignore')
    assert.equal(fsm.state, 'ignored')

    fsm.trigger('reset')
    assert.equal(fsm.state, 'pending')
  })
})
