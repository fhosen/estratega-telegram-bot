class FSM {
  constructor(initialState) {
    this.state = initialState
    this.transitionsFor = {}
    this.callbacks = {}
    this.defaultCallbacks = []
  }

  on(state, callback) {
    if (typeof(this.callbacks[state]) === 'undefined') {
      this.callbacks[state] = []
    }
    this.callbacks[state].push(callback)
  }

  onAny(callback) {
    this.defaultCallbacks.push(callback)
  }

  when(event, transitions) {
    this.transitionsFor[event] = transitions
  }

  trigger(event) {
    return this.doesTrigger(event) && this._change(event)
  }

  doesTrigger(event) {
    const ts = this.transitionsFor[event]
    return ts && ts.hasOwnProperty(this.state)
  }

  _change(event) {
    this.state = this.transitionsFor[event][this.state]
    const callbacks = (this.callbacks[this.state] || [])
      .concat(this.defaultCallbacks)
    for (const cb of callbacks) {
      cb(event)
    }
    return true
  }
}

module.exports = FSM
