//const FSM = require('./fsm')

class EstrategaBot {
  constructor(telegramBot) {
    this.bot = telegramBot
    this._configure_callbacks()
  }

  _configure_callbacks() {
    this.bot.on('/start', msg => {
      this.bot.sendMessage(msg.chat.id, 'Game started')
    })

    // Listen for any kind of message. There are different kinds of
    // messages.
    this.bot.on('message', msg => {
      // send a message to the chat acknowledging receipt of their message
      this.bot.sendMessage(msg.chat.id, 'Received your message')
    })
  }
}

module.exports = EstrategaBot
