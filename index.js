const TelegramBot = require('node-telegram-bot-api')
const EstrategaBot = require('./lib/estratega_bot')

const token = process.env.BOT_TOKEN

if (!token) {
  console.error("You must provide BOT_TOKEN")
  process.exit(1)
}

const bot = new TelegramBot(token, {polling: true})
const estrategaBot = new EstrategaBot(bot)

console.log('Ready.')
