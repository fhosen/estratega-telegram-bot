# Estratega

(spanish only)

## Reglas del juego

1. Se elige un orden de jugadores, al azar. El repartidor es el primero en la
   lista.
2. Se reparten las cartas a cada jugador. En cada juego se va alternando la
   cantidad de cartas de la siguente manera: 3-5-7-5-3-1
3. Cada jugador debe cantar cuantas tiradas va a ganar en este juego. Valores
   posibles: 0 hasta la cantidad de cartas que tiene.  El último jugador no
   puede cantar la diferencia del total con la suma de las cantidades cantadas.
4. Comienza el repartidor tirando una carta, y siguen los demás jugadores en el
   orden de la ronda.
5. Al terminar la ronda, el jugador que gana se lleva todas las cartas de la
   mesa
6. Comienza tirando una carta el jugador que ganó en la ronda anterior, y se
   repite 5
7. Al terminar el juego, todos los jugadores que acertaron la cantidad de
   tiradas ganadas se les suman 5 puntos + N puntos donde N son las tiradas
   ganadas.  Los que no acertaron, no suman puntos.
8. El jugador que llega primero a 100 puntos gana la partida.

### Valor de las cartas

1. Ancho de basto
2. Ancho de espadas
3. Cartas en este orden numérico: 12, 11, 10, ..., 2, 1 (oro y copas)
